package com.felipecorrea.desafioamcomapi.resource;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.felipecorrea.desafioamcomapi.dto.CidadeMaisDistanteDTO;
import com.felipecorrea.desafioamcomapi.dto.UFQuantidadeCidadeDTO;
import com.felipecorrea.desafioamcomapi.event.RecursoCriadoEvent;
import com.felipecorrea.desafioamcomapi.exceptionhandler.DesafioAMcomExceptionHandler.Erro;
import com.felipecorrea.desafioamcomapi.model.CidadeIbge;
import com.felipecorrea.desafioamcomapi.repository.CidadeIbgeRepository;
import com.felipecorrea.desafioamcomapi.repository.filter.CidadeIbgeFilter;
import com.felipecorrea.desafioamcomapi.service.CidadeIbgeService;
import com.felipecorrea.desafioamcomapi.service.exception.CidadeIbgeJaCadastradaException;

@RestController
@RequestMapping("/cidades")
public class CidadeIbgeResource {
	
	@Autowired
	private ApplicationEventPublisher publisher;

	@Autowired
	private CidadeIbgeRepository cidadeIbgeRepository; 
	
	@Autowired
	private CidadeIbgeService cidadeIbgeService;
	
	@Autowired
	private MessageSource messageSource;
	
	@PostMapping("/arquivo")
	public ResponseEntity<List<CidadeIbge>> uploadCSVCidadesIbge(@RequestParam MultipartFile arquivo) {
		return ResponseEntity.ok(cidadeIbgeService.salvarCidadesDoArquivo(arquivo));
	}
	
	@GetMapping
	public List<CidadeIbge> listarTodos() {
		return cidadeIbgeRepository.findAll();
	}
	
	@GetMapping("/filtro")
	public List<CidadeIbge> pesquisar(CidadeIbgeFilter cidadeIbgeFilter) {
		return cidadeIbgeRepository.filtrar(cidadeIbgeFilter);
	}
	
	@PostMapping
	public ResponseEntity<CidadeIbge> criarCidade(@Valid @RequestBody CidadeIbge cidadeIbge, HttpServletResponse response) {
		CidadeIbge cidadeIbgeSalva = cidadeIbgeService.salvar(cidadeIbge);
		publisher.publishEvent(new RecursoCriadoEvent(this, response, cidadeIbgeSalva.getIbgeId()));
		return ResponseEntity.status(HttpStatus.CREATED).body(cidadeIbgeSalva);
	}
	
	@GetMapping("/{ibgeId}")
	public ResponseEntity<CidadeIbge> buscarPeloIbgeId(@PathVariable Long ibgeId) {
		Optional<CidadeIbge> cidade = cidadeIbgeRepository.findById(ibgeId);
		return cidade.isPresent() ? ResponseEntity.ok(cidade.get()) : ResponseEntity.notFound().build();
	}
	
	@DeleteMapping("/{ibgeId}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void remover(@PathVariable Long ibgeId) {
		cidadeIbgeRepository.deleteById(ibgeId);
	}
	
	@PutMapping("/{ibgeId}")
	public ResponseEntity<CidadeIbge> atualizar(@PathVariable Long ibgeId, @Valid @RequestBody CidadeIbge cidadeIbge) {
		CidadeIbge cidadeIbgeSalva = cidadeIbgeService.atualizar(ibgeId, cidadeIbge);
		return ResponseEntity.ok(cidadeIbgeSalva);
	}
	
	@GetMapping("/capitais")
	@ResponseStatus(HttpStatus.OK)
	public List<CidadeIbge> buscarCapitaisOrdenadasPorNome() {
		return cidadeIbgeRepository.findByCapital(Boolean.TRUE)
				.stream()
				.sorted(Comparator.comparing(CidadeIbge::getName))
				.collect(Collectors.toList());
	}
	
	@GetMapping("/total")
	@ResponseStatus(HttpStatus.OK)
	public long totalDeRegistros() {
		return cidadeIbgeRepository.count();
	}
	
	@GetMapping("/totalCidadesPorUf/{uf}")
	@ResponseStatus(HttpStatus.OK)
	public long buscarQuantidadeDeCidadesPorUf(@PathVariable String uf) {
		return cidadeIbgeRepository.countByUf(uf);
	}
	
	@GetMapping("/cidadesPorUf/{uf}")
	@ResponseStatus(HttpStatus.OK)
	public List<String> buscarCidadesPorUf(@PathVariable String uf) {
		return cidadeIbgeRepository.buscarNomeCidadesPorUf(uf);
	}
	
	@GetMapping("/totalRegistrosPorColuna/{coluna}")
	@ResponseStatus(HttpStatus.OK)
	public long buscarQuantidadeRegistrosPorColuna(@PathVariable String coluna) {
		return cidadeIbgeRepository.totalRegistrosPorColunaDistinct(coluna);
	}
	
	@GetMapping("/UFMaisEMenosCidades")
	@ResponseStatus(HttpStatus.OK)
	public List<UFQuantidadeCidadeDTO> buscarUFComMaisEMenosCidades() {
		return cidadeIbgeRepository.buscarUFComMaisEMenosCidades();
	}
	
	@GetMapping("/maisDistantes")
	public ResponseEntity<CidadeMaisDistanteDTO> buscarCidadesMaisDistantes() {
		CidadeMaisDistanteDTO cidadeMaisDistanteDTO = cidadeIbgeRepository.buscarCidadesMaisDistantes();
		return cidadeMaisDistanteDTO != null ? ResponseEntity.ok(cidadeMaisDistanteDTO) : ResponseEntity.noContent().build();
	}
	
	@ExceptionHandler({ CidadeIbgeJaCadastradaException.class })
	public ResponseEntity<Object> handlePessoaInexistenteOuInativaException(CidadeIbgeJaCadastradaException ex) {
		String mensagemUsuario = messageSource.getMessage("cidadeIbge.ja-cadastrada", null, LocaleContextHolder.getLocale());
		String mensagemDesenvolvedor = ex.toString();
		List<Erro> erros = Arrays.asList(new Erro(mensagemUsuario, mensagemDesenvolvedor));
		return ResponseEntity.badRequest().body(erros);
	}
}
