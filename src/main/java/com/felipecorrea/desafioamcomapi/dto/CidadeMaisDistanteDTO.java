package com.felipecorrea.desafioamcomapi.dto;

import com.felipecorrea.desafioamcomapi.model.CidadeIbge;

public class CidadeMaisDistanteDTO {
	
	private CidadeIbge cidade1;
	
	private CidadeIbge cidade2;
	
	private double distancia;

	public CidadeIbge getCidade1() {
		return cidade1;
	}

	public void setCidade1(CidadeIbge cidade1) {
		this.cidade1 = cidade1;
	}

	public CidadeIbge getCidade2() {
		return cidade2;
	}

	public void setCidade2(CidadeIbge cidade2) {
		this.cidade2 = cidade2;
	}

	public double getDistancia() {
		return distancia;
	}

	public void setDistancia(double distancia) {
		this.distancia = distancia;
	}

}
