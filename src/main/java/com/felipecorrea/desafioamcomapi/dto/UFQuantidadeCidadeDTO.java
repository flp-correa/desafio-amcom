package com.felipecorrea.desafioamcomapi.dto;

import java.math.BigInteger;

public class UFQuantidadeCidadeDTO {

	private String uf;
	
	private BigInteger quantidade;
	
	public UFQuantidadeCidadeDTO() {}

	public UFQuantidadeCidadeDTO(String uf, BigInteger quantidade) {
		this.uf = uf;
		this.quantidade = quantidade;
	}

	public String getUf() {
		return uf;
	}

	public void setUf(String uf) {
		this.uf = uf;
	}

	public BigInteger getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(BigInteger quantidade) {
		this.quantidade = quantidade;
	}
}
