package com.felipecorrea.desafioamcomapi.service;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.felipecorrea.desafioamcomapi.model.CidadeIbge;
import com.felipecorrea.desafioamcomapi.repository.CidadeIbgeRepository;
import com.felipecorrea.desafioamcomapi.service.exception.CidadeIbgeJaCadastradaException;
import com.felipecorrea.desafioamcomapi.service.exception.ErroAoLerArquivoException;
import com.vividsolutions.jts.geom.Point;

import au.com.bytecode.opencsv.CSVReader;

@Service
public class CidadeIbgeService {

	@Autowired
	private CidadeIbgeRepository cidadeIbgeRepository;

	public CidadeIbge salvar(CidadeIbge cidadeIbge) {
		if (cidadeIbgeRepository.findById(cidadeIbge.getIbgeId()).isPresent()) {
			throw new CidadeIbgeJaCadastradaException();
		}
		
		cidadeIbge.setPoint((Point) cidadeIbge.wktToGeometry(String.format("POINT (%s %s)", cidadeIbge.getLon(), cidadeIbge.getLat())));
		
		return cidadeIbgeRepository.save(cidadeIbge);
	}
	
	public CidadeIbge atualizar(Long ibgeId, CidadeIbge cidadeIbge) {
		cidadeIbge.setPoint((Point) cidadeIbge.wktToGeometry(String.format("POINT (%s %s)", cidadeIbge.getLon(), cidadeIbge.getLat())));
		Optional<CidadeIbge> cidadeIbgeOpitional = cidadeIbgeRepository.findById(ibgeId);
		
		if (!cidadeIbgeOpitional.isPresent() || !cidadeIbge.getIbgeId().equals(ibgeId)) {
			throw new EmptyResultDataAccessException(1);
		}
		
		BeanUtils.copyProperties(cidadeIbge, cidadeIbgeOpitional.get(), "ibgeId");
		return cidadeIbgeRepository.save(cidadeIbgeOpitional.get());
	}

	public List<CidadeIbge> salvarCidadesDoArquivo(MultipartFile arquivo) {
		List<CidadeIbge> cidades = new ArrayList<>();
		try {
			CSVReader csvReader = new CSVReader(new InputStreamReader(arquivo.getInputStream()));
			
			String [] nextLine = csvReader.readNext();
			
			if (isArquivoCSVValido(nextLine)) {
				nextLine = new String[nextLine.length];
				while ((nextLine = csvReader.readNext()) != null) {
					cidades.add(new CidadeIbge(Long.valueOf(nextLine[0]), nextLine[1].trim(), nextLine[2].trim(), 
							Boolean.valueOf(nextLine[3]), nextLine[4].trim(), nextLine[5].trim(),
							nextLine[6].trim(), nextLine[7].trim(), nextLine[8].trim(),nextLine[9].trim()));
				}
			}
		    csvReader.close();
		} catch (IOException e) {
			throw new ErroAoLerArquivoException();
		}
		
		return cidadeIbgeRepository.saveAll(cidades);
	}

	private boolean isArquivoCSVValido(String[] colunas) {
		final List<String> colunasCSV = Arrays.asList("ibge_id", "uf", "name", "capital", "lon", "lat", "no_accents", "alternative_names", "microregion", "mesoregion");
		if (colunas.length == 10 && colunasCSV.equals(Arrays.asList(colunas))) {
			return true;
		}
		throw new ErroAoLerArquivoException();
	}
}
