package com.felipecorrea.desafioamcomapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DesafioAmcomApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(DesafioAmcomApiApplication.class, args);
	}
}
