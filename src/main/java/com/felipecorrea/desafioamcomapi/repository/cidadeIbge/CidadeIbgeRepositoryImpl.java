package com.felipecorrea.desafioamcomapi.repository.cidadeIbge;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.util.StringUtils;

import com.felipecorrea.desafioamcomapi.dto.CidadeMaisDistanteDTO;
import com.felipecorrea.desafioamcomapi.dto.UFQuantidadeCidadeDTO;
import com.felipecorrea.desafioamcomapi.model.CidadeIbge;
import com.felipecorrea.desafioamcomapi.repository.filter.CidadeIbgeFilter;

public class CidadeIbgeRepositoryImpl implements CidadeIbgeRepositoryQuery {
	
	@PersistenceContext
	private EntityManager manager;

	@Override
	public List<CidadeIbge> filtrar(CidadeIbgeFilter cidadeIbgeFilter) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<CidadeIbge> criteria = builder.createQuery(CidadeIbge.class);
		Root<CidadeIbge> root = criteria.from(CidadeIbge.class);
		
		Predicate[] predicates = criarRestricoes(cidadeIbgeFilter, builder, root);
		criteria.where(predicates);
		
		TypedQuery<CidadeIbge> query = manager.createQuery(criteria);
		List<CidadeIbge> cidades = query.getResultList();
		
		filtroLonELat(cidadeIbgeFilter, cidades);
		
		return cidades;
	}
	
	@Override
	public List<String> buscarNomeCidadesPorUf(String uf) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<String> criteria = builder.createQuery(String.class);
		Root<CidadeIbge> root = criteria.from(CidadeIbge.class);
		criteria.select(root.get("name"));
		
		criteria.where(builder.equal(builder.lower(root.get("uf")), uf.toLowerCase()));
		TypedQuery<String> query = manager.createQuery(criteria);
		return query.getResultList();
	}
	
	@Override
	public long totalRegistrosPorColunaDistinct(String coluna) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
		Root<CidadeIbge> root = criteria.from(CidadeIbge.class);
		criteria.select(builder.countDistinct((root.get(coluna))));
		
		TypedQuery<Long> query = manager.createQuery(criteria);
		return query.getSingleResult();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<UFQuantidadeCidadeDTO> buscarUFComMaisEMenosCidades() {
		String jpql = "SELECT c.uf, COUNT(c.uf) " +
						"FROM cidade_ibge as c GROUP BY " +
						"c.uf HAVING COUNT(c.uf) = ( SELECT MAX(c1) FROM (SELECT COUNT(cc.uf) AS c1 " +
														"FROM cidade_ibge as cc GROUP BY cc.uf) AS ci) " + 
						"OR COUNT(c.uf) = ( SELECT MIN(c1) FROM (SELECT COUNT(cc.uf) AS c1 " +
												"FROM cidade_ibge as cc GROUP BY cc.uf) AS ci2)";
		
		List<Object[]> listaUF = manager.createNativeQuery(jpql).getResultList();
		
		return listaUF.stream().map(l -> {
			return new UFQuantidadeCidadeDTO((String)l[0], (BigInteger)l[1]);
		}).collect(Collectors.toList());
	}
	
	@Override
	public CidadeMaisDistanteDTO buscarCidadesMaisDistantes() {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<CidadeIbge> criteria = builder.createQuery(CidadeIbge.class);
		Root<CidadeIbge> root = criteria.from(CidadeIbge.class);
		criteria.select(root);
		TypedQuery<CidadeIbge> query = manager.createQuery(criteria);
		
		List<CidadeIbge> cidades = query.getResultList();
		if (cidades.size() > 1) {
			return calcularCidadesMaisDistantes(cidades);
		}
		return null;
	}
	
	private CidadeMaisDistanteDTO calcularCidadesMaisDistantes(List<CidadeIbge> cidades) {
		CidadeMaisDistanteDTO cidadeMaisDistanteDTO = new CidadeMaisDistanteDTO();
		
		for(int i = 0; i < cidades.size(); i++) {
			final double ponto1 = Math.sqrt(calcularCoordenada(Double.valueOf(cidades.get(i).getLon()), Double.valueOf(cidades.get(0).getLon())));
			final double ponto2 = Math.sqrt(calcularCoordenada(Double.valueOf(cidades.get(i).getLat()), Double.valueOf(cidades.get(0).getLat())));
			final double distancia = Math.sqrt(ponto1 + ponto2);
			if (distancia > cidadeMaisDistanteDTO.getDistancia()) {
				cidadeMaisDistanteDTO.setCidade1(cidades.get(0));
				cidadeMaisDistanteDTO.setCidade2(cidades.get(i));
				cidadeMaisDistanteDTO.setDistancia(distancia);
			}
			if (i+1 == cidades.size()) {
				i = 0;
				cidades.remove(0);
			}
		}
		
		return cidadeMaisDistanteDTO;
	}
	
	private double calcularCoordenada(double x, double y) {
		return Math.pow(x - y, 2);
	}
	
	private void filtroLonELat(CidadeIbgeFilter cidadeIbgeFilter, List<CidadeIbge> cidades) {
		if (!StringUtils.isEmpty(cidadeIbgeFilter.getLat())) {
			cidades.removeIf(c -> c.getLat().equals(cidadeIbgeFilter.getLat()));
		}
		
		if (!StringUtils.isEmpty(cidadeIbgeFilter.getLon())) {
			cidades.removeIf(c -> c.getLon().equals(cidadeIbgeFilter.getLon()));
		}
	}

	private Predicate[] criarRestricoes(CidadeIbgeFilter cidadeIbgeFilter, CriteriaBuilder builder,
			Root<CidadeIbge> root) {
		
		List<Predicate> predicates = new ArrayList<>();
		
		if (!StringUtils.isEmpty(cidadeIbgeFilter.getAlternativeNames())) {
			predicates.add(builder.equal(builder.lower(root.get("alternativeNames")), cidadeIbgeFilter.getAlternativeNames().toLowerCase()));
		}
		
		if (!StringUtils.isEmpty(cidadeIbgeFilter.getCapital())) {
			predicates.add(builder.equal(root.get("capital"), cidadeIbgeFilter.getCapital()));
		}
		
		if (!StringUtils.isEmpty(cidadeIbgeFilter.getIbgeId())) {
			predicates.add(builder.equal(root.get("ibgeId"), cidadeIbgeFilter.getIbgeId()));
		}
		
		if (!StringUtils.isEmpty(cidadeIbgeFilter.getMesoregion())) {
			predicates.add(builder.equal(builder.lower(root.get("mesoregion")), cidadeIbgeFilter.getMesoregion().toLowerCase()));
		}
		
		if (!StringUtils.isEmpty(cidadeIbgeFilter.getMicroregion())) {
			predicates.add(builder.equal(builder.lower(root.get("microregion")), cidadeIbgeFilter.getMicroregion().toLowerCase()));
		}
		
		if (!StringUtils.isEmpty(cidadeIbgeFilter.getName())) {
			predicates.add(builder.equal(builder.lower(root.get("name")), cidadeIbgeFilter.getName().toLowerCase()));
		}
		
		if (!StringUtils.isEmpty(cidadeIbgeFilter.getNoAccents())) {
			predicates.add(builder.equal(builder.lower(root.get("noAccents")), cidadeIbgeFilter.getNoAccents().toLowerCase()));
		}
		
		if (!StringUtils.isEmpty(cidadeIbgeFilter.getUf()	)) {
			predicates.add(builder.equal(builder.lower(root.get("uf")), cidadeIbgeFilter.getUf().toLowerCase()));
		}
		
		return predicates.toArray(new Predicate[predicates.size()]);
	}
}
