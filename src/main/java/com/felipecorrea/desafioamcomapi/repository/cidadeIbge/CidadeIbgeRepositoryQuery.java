package com.felipecorrea.desafioamcomapi.repository.cidadeIbge;

import java.util.List;

import com.felipecorrea.desafioamcomapi.dto.CidadeMaisDistanteDTO;
import com.felipecorrea.desafioamcomapi.dto.UFQuantidadeCidadeDTO;
import com.felipecorrea.desafioamcomapi.model.CidadeIbge;
import com.felipecorrea.desafioamcomapi.repository.filter.CidadeIbgeFilter;

public interface CidadeIbgeRepositoryQuery {

	List<CidadeIbge> filtrar(CidadeIbgeFilter cidadeIbgeFilter);
	
	List<String> buscarNomeCidadesPorUf(String uf);
	
	long totalRegistrosPorColunaDistinct(String coluna);
	
	List<UFQuantidadeCidadeDTO> buscarUFComMaisEMenosCidades();
	
	CidadeMaisDistanteDTO buscarCidadesMaisDistantes();
}
