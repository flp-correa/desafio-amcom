package com.felipecorrea.desafioamcomapi.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.felipecorrea.desafioamcomapi.model.CidadeIbge;
import com.felipecorrea.desafioamcomapi.repository.cidadeIbge.CidadeIbgeRepositoryQuery;

public interface CidadeIbgeRepository extends JpaRepository<CidadeIbge, Long>, CidadeIbgeRepositoryQuery {
	
	List<CidadeIbge> findByCapital(Boolean opcao);
	
	long countByUf(String uf);
}
