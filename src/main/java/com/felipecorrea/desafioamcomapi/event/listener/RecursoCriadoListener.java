package com.felipecorrea.desafioamcomapi.event.listener;

import java.net.URI;

import javax.servlet.http.HttpServletResponse;

import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.felipecorrea.desafioamcomapi.event.RecursoCriadoEvent;

@Component
public class RecursoCriadoListener implements ApplicationListener<RecursoCriadoEvent> {

	@Override
	public void onApplicationEvent(RecursoCriadoEvent recursoCriadoEvent) {
		HttpServletResponse response = recursoCriadoEvent.getResponse();
		Long ibgeId = recursoCriadoEvent.getIbgeId();
		
		adicionarHeaderLocation(response, ibgeId);
	}

	private void adicionarHeaderLocation(HttpServletResponse response, Long ibgeId) {
		URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().path("/{ibgeId}")
				.buildAndExpand(ibgeId).toUri();
		response.setHeader("Location", uri.toASCIIString());
	}

}
