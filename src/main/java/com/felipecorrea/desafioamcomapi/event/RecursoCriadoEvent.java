package com.felipecorrea.desafioamcomapi.event;

import javax.servlet.http.HttpServletResponse;

import org.springframework.context.ApplicationEvent;

public class RecursoCriadoEvent extends ApplicationEvent {

	private static final long serialVersionUID = 1L;
	
	private HttpServletResponse response;
	private Long ibgeId;

	public RecursoCriadoEvent(Object source, HttpServletResponse response, Long ibgeId) {
		super(source);
		this.response = response;
		this.ibgeId = ibgeId;
	}

	public HttpServletResponse getResponse() {
		return response;
	}

	public Long getIbgeId() {
		return ibgeId;
	}
	
}
