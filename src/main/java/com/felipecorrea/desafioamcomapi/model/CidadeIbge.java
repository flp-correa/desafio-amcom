package com.felipecorrea.desafioamcomapi.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.PostLoad;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.bedatadriven.jackson.datatype.jts.serialization.GeometryDeserializer;
import com.bedatadriven.jackson.datatype.jts.serialization.GeometrySerializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.io.ParseException;
import com.vividsolutions.jts.io.WKTReader;

@Entity
@Table(name = "cidade_ibge")
public class CidadeIbge implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ibge_id")
	@NotNull
	private Long ibgeId;
	
	@Size(min = 2, max = 2)
	@NotNull
	private String uf;
	
	@Size(min = 2, max = 50)
	@NotNull
	private String name;
	
	@NotNull
	private Boolean capital;
	
	@JsonSerialize(using = GeometrySerializer.class)
    @JsonDeserialize(contentUsing = GeometryDeserializer.class)
	private Point point;
	
	@Size(min = 2, max = 50)
	@NotNull
	@Column(name = "no_accents")
	private String noAccents;
	
	@Column(name = "alternative_names")
	private String alternativeNames;
	
	@Size(min = 2, max = 50)
	@NotNull
	private String  microregion;
	
	@Size(min = 2, max = 50)
	@NotNull
	private String mesoregion;
	
	@NotBlank
	@Transient
	private String lon;
	
	@NotBlank
	@Transient
	private String lat;
	
	public CidadeIbge() {}
	
	public CidadeIbge(Long ibgeId, String uf, String name, Boolean capital, String lon, String lat,
			String noAccents, String alternativeNames, String microregion, String mesoregion) {
		this.ibgeId = ibgeId;
		this.uf = uf;
		this.name = name;
		this.capital = capital;
		this.noAccents = noAccents;
		this.alternativeNames = alternativeNames;
		this.microregion = microregion;
		this.mesoregion = mesoregion;
		this.lon = lon;
		this.lat = lat;
		setPoint((Point) wktToGeometry(String.format("POINT (%s %s)", getLon(), getLat())));
	}


	@PostLoad
	@PrePersist
	@PreUpdate
	public void prePersist() throws ParseException {
		this.lon = this.point.toString().replace("POINT (", "").split(" ", 0)[0];
		this.lat = this.point.toString().replace("POINT (", "").split(" ", 0)[1].replace(")", "");
	}
	
	public Long getIbgeId() {
		return ibgeId;
	}

	public void setIbgeId(Long ibgeId) {
		this.ibgeId = ibgeId;
	}

	public String getUf() {
		return uf;
	}

	public void setUf(String uf) {
		this.uf = uf;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getCapital() {
		return capital;
	}

	public void setCapital(Boolean capital) {
		this.capital = capital;
	}

	public Point getPoint() {
		return point;
	}

	public void setPoint(Point point) {
		this.point = point;
	}

	public String getNoAccents() {
		return noAccents;
	}

	public void setNoAccents(String noAccents) {
		this.noAccents = noAccents;
	}

	public String getAlternativeNames() {
		return alternativeNames;
	}

	public void setAlternativeNames(String alternativeNames) {
		this.alternativeNames = alternativeNames;
	}

	public String getMicroregion() {
		return microregion;
	}

	public void setMicroregion(String microregion) {
		this.microregion = microregion;
	}

	public String getMesoregion() {
		return mesoregion;
	}

	public void setMesoregion(String mesoregion) {
		this.mesoregion = mesoregion;
	}

	public String getLon() {
		return lon;
	}

	public void setLon(String lon) {
		this.lon = lon;
	}

	public String getLat() {
		return lat;
	}

	public void setLat(String lat) {
		this.lat = lat;
	}
	
	public Geometry wktToGeometry(String pointText) {
	    try {
			return new WKTReader().read(pointText);
		} catch (ParseException e) {
		}
		return null;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((ibgeId == null) ? 0 : ibgeId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CidadeIbge other = (CidadeIbge) obj;
		if (ibgeId == null) {
			if (other.ibgeId != null)
				return false;
		} else if (!ibgeId.equals(other.ibgeId))
			return false;
		return true;
	}
}
