CREATE TABLE cidade_ibge (
    ibge_id BIGINT(20) PRIMARY KEY,
    uf CHAR(2) NOT NULL,
    name VARCHAR(50) NOT NULL,
    capital BOOLEAN,
    point GEOMETRY NOT NULL,
    no_accents VARCHAR(50) NOT NULL,
    alternative_names VARCHAR(150),
    microregion VARCHAR(50) NOT NULL,
    mesoregion VARCHAR(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
