Instruções para acesso as recursos

1. Ler o arquivo CSV das cidades para a base de dados;
POST -> https://desafio-amcom.herokuapp.com/cidades/arquivo
Instruções enviando via Postman: Aba body, selecionar form-data, no campo key digitar "arquivo", selecionar e enviar.

2. Retornar somente as cidades que são capitais ordenadas por nome;
GET -> https://desafio-amcom.herokuapp.com/cidades/capitais

3. Retornar o nome do estado com a maior e menor quantidade de cidades e a quantidade de cidades;
GET -> https://desafio-amcom.herokuapp.com/cidades/UFMaisEMenosCidades

4. Retornar a quantidade de cidades por estado;
GET -> https://desafio-amcom.herokuapp.com/cidades/totalCidadesPorUf/{uf}

5. Obter os dados da cidade informando o id do IBGE;
GET -> https://desafio-amcom.herokuapp.com/cidades/{ibgeId}

6. Retornar o nome das cidades baseado em um estado selecionado;
GET -> https://desafio-amcom.herokuapp.com/cidades/cidadesPorUf/{uf}

7. Permitir adicionar uma nova Cidade;
POST -> https://desafio-amcom.herokuapp.com/cidades

8. Permitir deletar uma cidade;
DELETE -> https://desafio-amcom.herokuapp.com/cidades/{ibgeId}

9. Permitir selecionar uma coluna (do CSV) e através dela entrar com uma string para filtrar. retornar assim todos os objetos que contenham tal string;
GET -> https://desafio-amcom.herokuapp.com/cidades/filtro

Observação: permite filtrar mais de uma coluna, por exemplo: https://desafio-amcom.herokuapp.com/cidades/filtro?uf=sc&microregion=blumenau

10. Retornar a quantidade de registro baseado em uma coluna. Não deve contar itens iguais;
GET -> https://desafio-amcom.herokuapp.com/cidades/totalRegistrosPorColuna/{coluna}

11. Retornar a quantidade de registros total;
GET -> https://desafio-amcom.herokuapp.com/cidades/total

12. Dentre todas as cidades, obter as duas cidades mais distantes uma da outra com base na localização (distância em KM em linha reta);
GET -> https://desafio-amcom.herokuapp.com/cidades/maisDistantes
Observação: solução parcial, falta converter distancia para Km.

13. Atualizar cidade
PUT -> https://desafio-amcom.herokuapp.com/cidades/{ibgeId}
